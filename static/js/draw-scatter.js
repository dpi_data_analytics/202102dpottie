let chartDiv = document.getElementById("chart");
var svg = d3.select(chartDiv).append("svg")
async function drawScatter() {
  // 1. access the data
  const dataset = await d3.csv('static/data/orange_weather.csv')

  const rainAccessor = (d) => d.monthlyRain
  const tempAccessor = (d) => d.maxTemp

  const dateParser = d3.timeParse("%m/%Y")
  const xAccessor = (d) => dateParser(d.Date)


  // 2. create dims
  const width = d3.min([
    document.getElementById("chart").clientWidth * 0.9,
    document.getElementById("chart").clientHeight * 0.9,
    ])
  let dimensions = {
    width: width,
    height: width,
    margin: {
      top: 10,
      right: 10,
      bottom: 50,
      left: 50,
    },
  }
  dimensions.boundedWidth = dimensions.width -
    dimensions.margin.left -
    dimensions.margin.right
  dimensions.boundedHeight = dimensions.height -
    dimensions.margin.top -
    dimensions.margin.bottom

  // 3. draw canvas
  const wrapper = d3.select("#wrapper")
    .append("svg")
      .attr("width", dimensions.width)
      .attr("height", dimensions.height)

  const bounds = wrapper.append("g")
      .style("transform", `translate(${
      dimensions.margin.left
      }px, ${dimensions.margin.top}px)`)

  // create the scales
  const xScale = d3.scaleTime()
      .domain(d3.extent(dataset, xAccessor))
      .range([0, dimensions.boundedWidth])
  const rainScale = d3.scaleLinear()
      .domain(d3.extent(dataset, rainAccessor))
      .range([dimensions.boundedHeight, 0])
      .nice()
  const tempScale = d3.scaleLinear()
      .domain(d3.extent(dataset, tempAccessor))
      .range([dimensions.boundedHeight, 0])
      .nice()

  // 4. draw the data
  const dots = bounds.selectAll("circle")
    .data(dataset)
    .enter().append("circle")
      .attr("cx", (d)=>xScale(xAccessor(d)))
      .attr("cy", (d)=>rainScale(rainAccessor(d)))
      .attr("r", 4)
      .attr("fill", "steelblue")
  // draw perif
  const xAxisGenerator = d3.axisBottom()
    .scale(xScale)
  const xAxis = bounds.append("g")
    .call(xAxisGenerator)
      .style("transform", `translateY(${dimensions.boundedHeight}px)`)

  const yAxisGenerator = d3.axisLeft()
    .scale(rainScale)
  const yAxis = bounds.append("g")
    .call(yAxisGenerator)

}
drawScatter()
