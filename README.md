# When an orange is not an Orange  

## Selenium driver
wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz

tar xzf geckodriver-v0.24.0-linux64.tar.gz

 chmod +x geckodriver

 sudo mv geckodriver /usr/local/bin/

## Data from Orange AP and Orange OAI  
Use `scripts/access_bom.ipynb`

## D3 or dash?  
For inspiration see: https://github.com/ucg8j/awesome-dash 


## data
min max  
tenp at 9am
wind dir
speed
rain since 9 am

Design
| temp | rain |
| maxmin | wind |

side by side temp ie 3deg^ with the ^ red or blue change since yesterday hover reading
wind arrow with speed at tip  
max min line max top min bottom blue to red heu temp y axis
rain bar graph  
